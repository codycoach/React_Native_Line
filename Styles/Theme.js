import React, {StyleSheet} from 'react-native'

export default StyleSheet.create({
    center:{
        justifyContent:'center',
        alignItems:'center'
    },
    textSmall:{
        color:'#8F95A2',
        fontSize:12
    },
    moreText:{
        color:'#343C50',
        fontSize:13,
        fontWeight:'500',
        marginTop:10
    },
    friendService:{
        height:50,
        width:50
    },
    // TextColor
    colorGreen:{
        color:'#53B535'
    },
    colorPrimary:{
        color:'#343C50'
    },
    colorGrey:{
        color:'#8F95A2'
    },
    mainTextHeader:{
        fontSize:18,
        color:'whitesmoke'
    },

    // BackgroundColor
    backgroundPrimary:{
        backgroundColor: '#343C50'
    },
    backgroundGrey:{
        backgroundColor:'#e6efef'
    },
    backgroundGreen:{
        backgroundColor:'#53B535'
    }
});
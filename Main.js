import React, { Component } from 'react';
import { Container, Header, Tab, Tabs, TabHeading, Icon, Text, Left, Right, StyleProvider ,View,Thumbnail, Badge} from 'native-base';
import { Image,NavigatorIOS } from 'react-native';
import Friends from './src/Tabs/Friends';
import Chats from './src/Tabs/Chats';
import Timeline from './src/Tabs/Timeline';
import LineToday from './src/Tabs/LineToday';
import More from './src/Tabs/More';
import Theme from './Styles/Theme';
import MCI from 'react-native-vector-icons/MaterialCommunityIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import Octicons from 'react-native-vector-icons/Octicons';
import SLI from 'react-native-vector-icons/SimpleLineIcons';
// import Nav from 'react-navigation';
export default class Main extends Component {
  static navigationOptions = {
    header: null
}
  constructor(props){
    super(props)
    this.state={
      tabSelected:0
    }
  }
  _changeTab(index){
    this.setState({
      tabSelected:index
    });
  }
  render() {
    let {tabSelected} = this.state
    return (
      <Container>
        {tabSelected==0?<FriendsHeader navigation={this.props.navigation}/>
            :tabSelected==1?<ChatsHeader/>
            :tabSelected==2?<TimelineHeader/>
            :tabSelected==3?<LineTodayHeader/>
            :<MoreHeader navigation={this.props.navigation}/>
        }
        {/* Friends */}
        <Tabs onChangeTab={(index)=>this._changeTab(index.i)} locked initialPage={this.state.tabIndex} tabBarUnderlineStyle={{ backgroundColor: 'white' }}>
          <Tab heading={
            <TabHeading style={Theme.backgroundPrimary}>
              {tabSelected==0?
                <Image style={{width:25,height:25,tintColor:'white'}} source={require('./src/Resources/Icon/tabs_friends.png')}/>
                :<Image style={{width:25,height:25}} source={require('./src/Resources/Icon/tabs_friends.png')}/>
              }
            </TabHeading>}>
            <Friends />
          </Tab>
          {/* Chats */}
          <Tab heading={
            <TabHeading style={Theme.backgroundPrimary}>
                {tabSelected==1?
                  <Image style={{width:25,height:25,tintColor:'white'}} source={require('./src/Resources/Icon/tabs_chats.png')}/>
                  :<Image style={{width:25,height:25}} source={require('./src/Resources/Icon/tabs_chats.png')}/>
                }
              <View>
                <Badge style={{position:'absolute',backgroundColor:'#DC4F2C',height:18,marginTop:-20,marginLeft:-5,paddingBottom:4,paddingTop:0,paddingLeft:2,paddingRight:2}}>
                  <Text style={{fontSize:12}}>6</Text>
                </Badge>
              </View>
            </TabHeading>}>
            <Chats />
          </Tab>
          {/* Timeline */}
          <Tab heading={
            <TabHeading style={Theme.backgroundPrimary}>
              {tabSelected==2?
                <Image style={{width:25,height:25,tintColor:'white'}} source={require('./src/Resources/Icon/tabs_timeline.png')}/>
                :<Image style={{width:25,height:25}} source={require('./src/Resources/Icon/tabs_timeline.png')}/>
              }
            </TabHeading>}>
            <Timeline />
          </Tab>
          {/* Linetoday */}
          <Tab heading={
            <TabHeading style={Theme.backgroundPrimary}>
              {tabSelected==3?
                <Image style={{width:25,height:25,tintColor:'white'}} source={require('./src/Resources/Icon/tabs_line_today.png')}/>
                :<Image style={{width:25,height:25}} source={require('./src/Resources/Icon/tabs_line_today.png')}/>
              }
            </TabHeading>}>
            <LineToday />
          </Tab>
          {/* More */}
          <Tab heading={
            <TabHeading style={Theme.backgroundPrimary}>
              {tabSelected==4?
                <Entypo name='dots-three-horizontal' size={25} color='white'/>
                :<Entypo name='dots-three-horizontal' size={25} color='slategrey'/>
              }
          </TabHeading>}>
            <More navigation={this.props.navigation}/>
          </Tab>
        </Tabs>
      </Container>
    );
  }
}

class FriendsHeader extends Component {
  render() {
    let {navigation} = this.props;  
    return (
      <Header hasTabs style={Theme.backgroundPrimary}>
        <Left>
          <Text style={Theme.mainTextHeader}>Friends 4</Text>
        </Left>
        <Right>
          <Image style={{width:20,height:20}} source={require('./src/Resources/Icon/friends_tabs_add_friends.png')}/>
          <Octicons style={{marginLeft:30}} name='search' color='whitesmoke' size={20}/>
          <MCI style={{marginLeft:30}} name='settings' color='whitesmoke' size={20} 
            onPress={() => {navigation.navigate('Setting')}}/>
        </Right>
      </Header>
    );
  }
}

class ChatsHeader extends Component {
  render() {
    return (
      <Header hasTabs style={Theme.backgroundPrimary}>
        <Left>
          <Text style={Theme.mainTextHeader}>Chats</Text>
        </Left>
        <Right>
          <Image style={{width:20,height:20}} source={require('./src/Resources/Icon/chat_plus.png')}/>
          <Octicons style={{marginLeft:30}} name='search' color='whitesmoke' size={20}/>
          <Entypo style={{marginLeft:30}} name='dots-three-vertical' color='whitesmoke' size={20}/>
        </Right>
      </Header>
    );
  }
}

class TimelineHeader extends Component {
  render() {
    return (
      <Header hasTabs style={Theme.backgroundPrimary}>
        <Left>
          <Text style={Theme.mainTextHeader}>Timeline</Text>
        </Left>
        <Right style={{alignItems:'center'}}>
          <MCI name='bell' color='whitesmoke' size={20}/>
          <Image style={{width:17,height:17,marginLeft:30}} source={require('./src/Resources/Icon/timeline_group.png')}/>
          <MCI style={{marginLeft:30}} name='pencil' color='whitesmoke' size={20}/>
        </Right>
      </Header>
    );
  }
}

class LineTodayHeader extends Component {
  render() {
    return (
      <Header hasTabs style={Theme.backgroundPrimary}>
        <Left>
          <Text style={Theme.mainTextHeader}>LINE TODAY</Text>
        </Left>
        <Right>
          <SLI name='menu' color='whitesmoke' size={20}/>
        </Right>
      </Header>
    );
  }
}

class MoreHeader extends Component {
  render() {
    let {navigation} = this.props;
    return (
      <Header hasTabs style={Theme.backgroundPrimary}>
        <Left>
          <Text style={Theme.mainTextHeader}>More</Text>
        </Left>
        <Right style={{alignItems:'flex-end'}}>
          <MCI name='settings' color='whitesmoke' size={20}
            onPress={() => {navigation.navigate('Setting')}}/>
        </Right>
      </Header>
    );
  }
}
import React, { Component } from "react";
import Setting from './src/Pages/Setting';
import Profile from './src/Pages/Profile';
import Help from './src/Pages/Help';
import Theme from './src/Pages/Setting-Theme';
import Keep from './src/Pages/Keep';
import MyTheme from './src/Pages/MyTheme';
import Main from './Main';
import ThemeDetail from './src/Pages/ThemeDetail';
import {StackNavigator} from 'react-navigation';

const RootStack = StackNavigator(
    {
      Home:{
        screen: Main,
      },
      Profile:{
        screen: Profile,
      },
      Help:{
          screen: Help,
      },
      Theme:{
          screen: Theme,
      },
      Keep:{
          screen:Keep,
      },
      MyTheme:{
          screen:MyTheme,
      },
      Setting:{
          screen:Setting,
      },
      ThemeDetail:{
          screen:ThemeDetail,
      },
    },
    {
      initialRouteName: 'Home',
      /* The header config from HomeScreen is now here */
      navigationOptions: {
        headerStyle: {
          backgroundColor: '#343C50',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight:'normal'
        },
      },
    }
  ) ;

  export default class App extends Component {
    render() {
      return (  
        <RootStack />
      );
    }
  }
  

import React, { Component } from 'react';
import { Container, Header, Content, Card, CardItem, Text, Icon, Right, Separator, List, ListItem, Thumbnail } from 'native-base';
import { Image, StyleSheet } from 'react-native';

export default class Setting extends Component {
    constructor(props){
        super(props);
    }
    static navigationOptions = {
        title: 'ตั้งค่า',
        headerLeft:null,
      }
    render() {
        return (
            <Container>
                <Content>

                    <Separator style={styles.Separator}>
                        <Text style={styles.textSeparator}>ข้อมูลของฉัน</Text>
                    </Separator>
                    <ListItem noBorder onPress={() => {this.props.navigation.navigate('Profile')}}>
                        <Image source={require('../Resources/Icon/profile.png')} style={styles.icon} />
                        <Text style={styles.label}>โปรไฟล์</Text>
                    </ListItem>
                    <ListItem noBorder>
                        <Image source={require('../Resources/Icon/account.png')} style={styles.icon} />
                        <Text style={styles.label}>บัญชี</Text>
                    </ListItem>
                    <ListItem noBorder>
                        <Image source={require('../Resources/Icon/privacy.png')} style={styles.icon} />
                        <Text style={styles.label}>ความเป็นส่วนตัว</Text>
                    </ListItem>
                    <ListItem>
                        <Image source={require('../Resources/Icon/keep.png')} style={styles.icon} />
                        <Text style={styles.label}>Keep</Text>
                    </ListItem>

                    <Separator style={styles.Separator}>
                        <Text style={styles.textSeparator}>ร้าน</Text>
                    </Separator>
                    <ListItem noBorder >
                        <Image source={require('../Resources/Icon/sticker.png')} style={styles.icon} />
                        <Text style={styles.label}>สติกเกอร์</Text>
                    </ListItem>
                    <ListItem noBorder onPress={() => {this.props.navigation.navigate('Theme')}}>
                        <Image source={require('../Resources/Icon/theme.png')} style={styles.icon} />
                        <Text style={styles.label}>ธีม</Text>
                    </ListItem>
                    <ListItem>
                        <Image source={require('../Resources/Icon/coin.png')} style={styles.icon} />
                        <Text style={styles.label}>เหรียญ</Text>
                    </ListItem>

                    <Separator style={styles.Separator}>
                        <Text style={styles.textSeparator}>ตั้งค่าพื้นฐาน</Text>
                    </Separator>
                    <ListItem noBorder >
                        <Image source={require('../Resources/Icon/noti.png')} style={styles.icon} />
                        <Text style={styles.label}>การแจ้งเตือน</Text>
                    </ListItem>
                    <ListItem noBorder >
                        <Image source={require('../Resources/Icon/pic.png')} style={styles.icon} />
                        <Text style={styles.label}>รูป & วิดีโอ</Text>
                    </ListItem>
                    <ListItem noBorder >
                        <Image source={require('../Resources/Icon/chat.png')} style={styles.icon} />
                        <Text style={styles.label}>แชท</Text>
                    </ListItem>
                    <ListItem noBorder >
                        <Image source={require('../Resources/Icon/call.png')} style={styles.icon} />
                        <Text style={styles.label}>โทร</Text>
                    </ListItem>
                    <ListItem noBorder >
                        <Image source={require('../Resources/Icon/lineout.png')} style={styles.icon} />
                        <Text style={styles.label}>LINE Out</Text>
                    </ListItem>
                    <ListItem noBorder >
                        <Image source={require('../Resources/Icon/friend.png')} style={styles.icon} />
                        <Text style={styles.label}>เพื่อน</Text>
                    </ListItem>
                    <ListItem noBorder >
                        <Image source={require('../Resources/Icon/group.png')} style={styles.icon} />
                        <Text style={styles.label}>กลุ่ม</Text>
                    </ListItem>
                    <ListItem noBorder >
                        <Image source={require('../Resources/Icon/timeline.png')} style={styles.icon} />
                        <Text style={styles.label}>ไทม์ไลน์</Text>
                    </ListItem>
                    <ListItem>
                        <Image source={require('../Resources/Icon/language.png')} style={styles.icon} />
                        <Text style={styles.label}>ภาษา</Text>
                    </ListItem>

                    <Separator style={styles.Separator}>
                        <Text style={styles.textSeparator}>ประกาศและอื่นๆ</Text>
                    </Separator>
                    <ListItem noBorder >
                        <Image source={require('../Resources/Icon/annoucement.png')} style={styles.icon} />
                        <Text style={styles.label}>ประกาศ</Text>
                    </ListItem>
                    <ListItem noBorder onPress={() => {this.props.navigation.navigate('Help')}} >
                        <Image source={require('../Resources/Icon/help.png')} style={styles.icon} />
                        <Text style={styles.label}>ช่วยเหลือ</Text>
                    </ListItem>
                    <ListItem>
                        <Image source={require('../Resources/Icon/about.png')} style={styles.icon} />
                        <Text style={styles.label}>เกี่ยวกับ LINE</Text>
                    </ListItem>

                </Content>
            </Container>
        )
    }
}



const styles = StyleSheet.create({
    icon: {
        width: 25,
        height: 25
    },
    label: {
        paddingLeft: 20
    },
    Separator: {
        backgroundColor: 'transparent',
        marginTop: 20
    },
    textSeparator:{
        color: 'darkblue', 
        fontSize: 12 
    },
    header:{
        backgroundColor:'#00264d',
    }

})
import React, { Component } from 'react';
import { Container, Header, Content, Card, CardItem, Text, Right, Separator, List, ListItem, Thumbnail, Left, Body, Button, CheckBox } from 'native-base';
import { Image, StyleSheet, View } from 'react-native';
import MCI from 'react-native-vector-icons/MaterialCommunityIcons';
import Entypo from 'react-native-vector-icons/Entypo';
export default class Setting extends Component {
    static navigationOptions = {
        title: 'โปรไฟล์',
        headerLeft: null
    }
    render() {
        return (
            <Container>
                <Content>
                    <CardItem style={{ backgroundColor: '#EFEEEC' }}>
                        <View style={{ flex: 1.3, alignItems: 'flex-start', justifyContent: 'center' }}>
                            <Thumbnail source={require('../Resources/Image/user.jpg')} />
                        </View>
                        <View style={{ flex: 4}}>
                            <Text style={{ color: 'grey' }}>หมายเลขโทรศัพท์</Text>
                            <Text style={[styles.user_information,{paddingTop:3,paddingBottom:5}]}>+66 91 999 9999</Text>
                            <View style={{ flexDirection: 'row' }}>
                                <Button style={{backgroundColor:'white',alignItems:'center',width:100,height:30}}>
                                <Entypo name='home' color='#2e2f4d' backgroundColor='white'
                                    borderRadius={1} size={14} style={{marginHorizontal:15}}>หน้าหลัก</Entypo>
                                </Button>
                                <Text>  </Text>
                                <Button style={{backgroundColor:'white',alignItems:'center',width:100,height:30}}>
                                <MCI name='arrow-down-bold-hexagon-outline' color='#2e2f4d' backgroundColor='whitesmoke'
                                    borderRadius={1} size={14} style={{marginHorizontal:15}}
                                    onPress={() => {this.props.navigation.navigate('Keep')}}>Keep</MCI>
                                </Button>
                            </View>
                        </View>
                    </CardItem>

                    <List>
                        <ListItem style={{ flex: 1 }}>
                            <View style={{ flex: 10 }}>
                                <Text style={styles.textLabel}>แชร์รูปโปรไฟล์</Text>
                                <Text note style={styles.textLabel}>แชร์บนไทม์ไลน์เมื่อคุณเปลี่ยนรูปโปรไฟล์</Text>
                            </View>
                            <View style={{ flex: 1 }}>
                                <CheckBox checked style={{ alignSelf: 'flex-end', backgroundColor: '#00c300', borderColor: 'transparent' }} />
                            </View>
                        </ListItem>

                        <ListItem style={styles.label}>
                            <Text style={styles.textLabel}>ชื่อ</Text>
                            <Text style={styles.user_information}>Coach</Text>
                        </ListItem>

                        <ListItem style={styles.label}>
                            <Text style={styles.textLabel}>สถานะ</Text>
                            {/* <Text note style={styles.textLabel}>ยังไม่ได้ตั้งค่า</Text> */}
                            <Text style={styles.user_information}>love</Text>
                        </ListItem>

                        <ListItem style={styles.label}>
                            <Text style={styles.textLabel}>LINE ID</Text>
                            <Text style={styles.user_information}>coach_etn</Text>
                        </ListItem>

                        <ListItem style={{ flex: 1 }}>
                            <View style={{ flex: 10 }}>
                                <Text style={styles.textLabel}>อนุญาตให้เพิ่มเป็นเพื่อนด้วย ID</Text>
                                <Text note style={styles.textLabel}>ผู้อื่นสามารถค้นหาและเพิ่มคุณเป็นเพื่อนด้วย ID</Text>
                            </View>
                            <View style={{ flex: 1 }}>
                                <CheckBox style={{ alignSelf: 'flex-end', borderColor: 'grey' }} />
                            </View>
                        </ListItem>

                        <ListItem style={styles.label}>
                            <Text style={styles.textLabel}>คิวอาร์โค้ด</Text>
                        </ListItem>

                        <ListItem last style={styles.label}>
                            <Text style={styles.textLabel}>วันเกิด</Text>
                            <Text note style={styles.textLabel}>ยังไม่ได้ตั้งค่า</Text>
                        </ListItem>
                    </List>
                </Content>
            </Container>
        )
    }
}



const styles = StyleSheet.create({
    icon: {
        width: 25,
        height: 25,
    },
    label: {
        flexDirection: 'column',
        flex: 1
    },
    textLabel: {
        alignSelf: 'flex-start'
    },
    user_information: {
        color: 'darkblue',
        alignSelf: 'flex-start'
    },
    button: {
        paddingTop: 20,
    }

})
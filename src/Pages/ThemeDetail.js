import React, { Component } from 'react';
import { Container, Header, H3, Tab, Tabs, TabHeading, Icon, Text, Left, Right, List
        , ListItem, View, Content, Thumbnail, Button, Body,Footer } from 'native-base';
import { Image, Alert, TouchableHighlight, Picker, StyleSheet,ScrollView } from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import Theme from '../../Styles/Theme';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MCI from 'react-native-vector-icons/MaterialCommunityIcons';

export default class ThemeDetail extends Component {
    static navigationOptions = {
        title: 'รายละเอียดธีม',
        headerLeft: null,
        headerRight: <Entypo name='share'color='whitesmoke' size={26} style={{marginRight:20}}/>,
    }
    render() {
        return (
            <Container>
                <View>

                    <ListItem noBorder style={{ flexDirection: 'row', alignContent: 'stretch' }}>
                        <View>
                            <Thumbnail square style={styles.icon} source={require('../Resources/Image/theme1-1.png')} />
                        </View>
                        <Body>
                            <View style={{ marginLeft: 20, borderBottomWidth: 0.5, borderBottomColor: '#D3D3D3', paddingBottom: 7 }}>
                                <Text style={{ fontWeight: 'bold', fontSize: 12 }}>LINE</Text>
                                <Text style={{ fontWeight: 'bold' }}>ดั้งเดิม</Text>
                                <Text note style={{ fontSize: 12, marginTop: 2 }}>ไม่มีวันหมดอายุการใช้งาน</Text>

                            </View>
                            <Text style={{ color: 'red', fontSize: 16, fontWeight: 'bold', marginTop: 5, marginLeft: 20 }}>ฟรี</Text>
                        </Body>
                    </ListItem>
                    <View style={{
                        marginHorizontal: 20, paddingVertical: 8, alignItems: 'center',
                        borderColor: '#8F95A2', borderWidth: 0.2, backgroundColor: '#D3D3D3'
                    }}>
                        <Text style={{ fontWeight: '300', fontSize: 13, color: '#8F95A2' }}>ใช้อยู่ </Text>
                    </View>
                    <View style={{ borderBottomColor: '#D3D3D3', borderBottomWidth: 0.5, marginTop: 20 }}></View>

                    <View style={{ paddingHorizontal: 20, marginTop: 20 }}>
                        <Text style={{ fontWeight: 'bold', marginBottom: 15, fontSize: 13, color: '#8F95A2' }}>
                            ธีม LINE แบบออริจินัลสุดเก๋</Text>
                        <Text style={{ fontSize: 13, color: '#8F95A2' }}>
                            บางส่วนของธีมอาจแสดงอย่างไม่ถูกต้องบน LINE เวอร์ชันที่คุณใช้อยู่ในขณะนี้
                        </Text>
                        <ScrollView horizontal style={{ marginTop:15,marginLeft: -10 }}>
                        <Image style={styles.image}source={require('../Resources/Image/theme1-1.png')}/>
                        <Image style={styles.image}source={require('../Resources/Image/theme1-2.png')}/>
                        <Image style={styles.image}source={require('../Resources/Image/theme1-3.png')}/>
                        <Image style={styles.image}source={require('../Resources/Image/theme1-4.png')}/>
                        <Image style={styles.image}source={require('../Resources/Image/theme1-5.png')}/>
                        </ScrollView>
                    </View>
                </View>
                <Footer style={{flexDirection:'column',alignItems:'center',justifyContent:'flex-end',backgroundColor:'transparent'
                                ,paddingBottom:-20,height:65}}>
                    <MCI name='copyright' note>LINE Corporation</MCI>
                </Footer>
                
            </Container>
        )
    }
}
const styles = StyleSheet.create({
    icon: {
        width: 90,
        height: 130,
    },
    image:{
        width:120,
        height:200,
        margin:5   
    }
})
import React, { Component } from 'react';
import { Container, Header, Content, Text, Right, Separator, List, ListItem, Thumbnail, Left, Body, Button, Icon, Item, Input } from 'native-base';
import { Image, StyleSheet, View, TouchableOpacity, ScrollView } from 'react-native';
import Theme from '../../Styles/Theme';
import MCI from 'react-native-vector-icons/MaterialCommunityIcons';
import FA from 'react-native-vector-icons/FontAwesome';
export default class Help extends Component {
    static navigationOptions = {
        title: 'ช่วยเหลือ',
        headerLeft: null
    }
    render() {
        return (
            <Container>
                <Content>
                    <ListItem searchBar>
                        <Item style={{paddingLeft:10}}>
                            <Icon name="ios-search" />
                            <Input placeholder="ค้นหา" />
                        </Item>
                    </ListItem>
                    <ListItem style={{}}>
                        <Text style={styles.textSeparator}>คำถามที่ถามบ่อย</Text>
                    </ListItem>
                    <Collab />
                    <Separator style={styles.separator}>
                        <Text style={styles.textSeparator}>Category</Text>
                    </Separator>
                    <ListItem noBorder>
                        <Image source={require('../Resources/Icon/grey-line.png')} style={[styles.icon]} />
                        <Text style={styles.label}>ลงทะเบียนใหม่, โอนย้ายข้อมูลบัญชี</Text>
                    </ListItem>
                    <ListItem noBorder>
                        <Image source={require('../Resources/Icon/account.png')} style={styles.icon} />
                        <Text style={styles.label}>การตรวจสอบ, เปลี่ยนแปลงข้อมูลการลงทะเบียน</Text>
                    </ListItem>
                </Content>
            </Container>
        )
    }
}

class Collab extends Component {
    constructor(props) {
        super(props)
        this.state = {
            errDisplay: false,
        }
    }
    _collapsibleErrDisplay() {
        if (!this.state.errDisplay) {
            this.setState({
                errDisplay: true
            })
        } else {
            this.setState({
                errDisplay: false
            })
        }
    }

    render() {
        return (
            <View style={{ borderBottomWidth: 1, borderColor: 'gainsboro', marginLeft: 10 }}>
                <TouchableOpacity activeOpacity={1} onPress={() => this._collapsibleErrDisplay()} style={{ flexDirection: 'row', padding: 5 }}>
                    <View style={{ flex: 20, marginTop: 5, paddingBottom: 10 }}>
                        <Text>
                            หน้าจอของ LINE และ แอพพลิเคชันอื่นที่เกี่ยวข้องแสดงไม่ปกติ</Text>
                    </View>
                    <View style={{ flex: 1, alignContent: 'center', justifyContent: 'center' }}>
                        <FA size={20} style={this.state.errDisplay ? Theme.colorGrey : Theme.colorGreen} name={this.state.errDisplay ? 'angle-up' : 'angle-down'} />
                    </View>
                </TouchableOpacity>
                {this.state.errDisplay ? <ServiceCollab /> : null}
            </View>
        )
    }

}


class ServiceCollab extends Component {
    render() {
        return (
            <View style={{ paddingLeft: 5, paddingTop: 15, backgroundColor: '#EFEEEC', paddingRight: 30, paddingBottom: 30 }}>
                <Text style={{ fontWeight: 'bold', fontSize: 13, color: '#6D6C6A' }}>สำหรับผู้ใช้ระบบปฏิบัติการณ์ Android</Text>
                <Text style={{ fontSize: 13, color: '#6D6C6A', marginTop: 3 }}>
                    กรณีที่ LINE และเพจบางส่วนในแอพลิเคชั่นอื่นที่เกี่ยวข้องแสดงไม่ปกติ กรุราลองแก้ไขตามคำแนะนำดังนี้
                </Text>
                <Text style={{ fontSize: 13, color: '#6D6C6A', marginTop: 20 }}>
                    1. ติดตั้ง Android System WebView เวอร์ชันล่าสุดจาก Google Play {'\n'}
                    2. หากยังพบปัญหาอยู่แม้จะจากติดตั้ง Android System WebView แล้วก็ตาม กรุณาอัปเดต Chrome Browser
                </Text>
                <Text style={{ fontSize: 13, color: '#6D6C6A', marginTop: 20 }}>
                    - <Text style={{ color: 'darkblue', textDecorationLine: 'underline', fontSize: 13 }}>Android System WebView</Text>
                </Text>
                <Text style={{ fontSize: 13, color: '#6D6C6A', marginTop: 20 }}>
                    - <Text style={{ color: 'darkblue', textDecorationLine: 'underline', fontSize: 13 }}>Chrome Browser</Text>
                </Text>
                <FooterItem />
            </View>
        );
    }
}

class FooterItem extends Component {
    render() {
        return (
            <View>
                <Text style={{ fontSize: 13, color: '#6D6C6A', marginTop: 40 }}>
                    หน้านี้เป็นประโยชน์ต่อคุณหรือไม่
                </Text>
                <View style={{ flexDirection: 'row', padding: 10 }}>
                    <Button style={{ backgroundColor: 'white', width: 80, height: 30 }}>
                        <Text style={{ color: '#3B3A38', fontSize: 13, paddingLeft: 30 }}>ใช่</Text>
                    </Button>
                    <Text>  </Text>
                    <Button style={{ backgroundColor: 'white', width: 80, height: 30, }}>
                        <Text style={{ color: '#3B3A38', fontSize: 13, paddingLeft: 25 }}>ไม่ใช่</Text>
                    </Button>
                    <Right>
                        <Image style={{ height: 18, width: 70 }}
                            source={require('../Resources/Icon/line-it.png')} />
                    </Right>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    icon: {
        width: 25,
        height: 25,

    },
    label: {
        paddingLeft:15,
        paddingRight:45
    },
    textLabel: {
        alignSelf: 'flex-start'
    },
    user_information: {
        color: 'darkblue',
        alignSelf: 'flex-start'
    },
    button: {
        paddingTop: 20,
    },
    separator: {
        backgroundColor: 'transparent',
        marginTop: 20,
    },
    textSeparator: {
        fontWeight: 'bold',
        fontSize: 16,
        color: '#6D6C6A'
    },

})
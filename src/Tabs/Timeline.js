import React, { Component } from 'react';
import {Content, Container, Header, Tab, Tabs, TabHeading, Icon, Text ,Left,Right,List,ListItem,View,Thumbnail, Button} from 'native-base';
import FA from 'react-native-vector-icons/FontAwesome';
import MCI from 'react-native-vector-icons/MaterialCommunityIcons';
import Theme from './../../Styles/Theme';
import { Image } from 'react-native';
export default class Timeline extends Component {
    render() {
        return (
            <Content>
                <View style={{flexDirection:'row',alignItems:'center',padding:15,paddingBottom:0}}>
                    <Thumbnail small source={require('./../Resources/Image/user.jpg')}/>
                    <Text note> What's new?</Text>
                </View>
                <View style={{flexDirection:'row',alignItems:'center',borderBottomColor:'gainsboro',borderBottomWidth:1}}>
                    <Button transparent iconLeft style={{flex:1}}>
                        <Text style={[Theme.colorPrimary,{fontSize:15}]}><FA name="camera" size={15}/> Camera</Text>
                    </Button>
                    <Button transparent iconLeft style={{flex:2,justifyContent:'center'}}>
                        <Text style={[Theme.colorPrimary,{fontSize:15}]}><FA name="smile-o" size={15}/> Stickers</Text>
                    </Button>
                    <Button transparent iconLeft style={{flex:1}}>
                        <Text style={[Theme.colorPrimary,{fontSize:15}]}><MCI name="shape-square-plus" size={15}/> Relay</Text>
                    </Button>
                </View>
                <Post userPhoto={require('./../Resources/Image/tlek.jpg')} userName='Tlek Metta' postPhoto={require('./../Resources/Image/tlek_post.jpg')} postText='อยากไปเที่ยวจัง อิอิ'/>
                <Post userPhoto={require('./../Resources/Image/tide.jpg')} userName='Keattiyos Yothinraungrong' postPhoto={require('./../Resources/Image/tide_post.jpg')} postText='เบื่อหน้าฝน อยากเห็นหน้าเธอ'/>
            </Content>
        );
    }
}

class Post extends Component {
    render() {
        let {userPhoto,userName,postPhoto,postText} = this.props
        return (
            <View style={{marginBottom:10}}>
                <View style={{flexDirection:'row',alignItems:'center',padding:10}}>
                    <Thumbnail small source={userPhoto}/>
                    <Text style={{fontWeight:'700'}}> {userName}</Text>
                    <View style={{justifyContent:'center',flex:1}}>
                        <Text style={{textAlign:'right'}}><MCI size={20} name="dots-horizontal"/></Text>
                    </View>
                </View>
                <Image style={{flex:1,height:300,width:null}} source={postPhoto}/>
                <Text style={{padding:10}}>{postText}</Text>
                <View style={{flexDirection:'row',padding:10,paddingTop:0,alignItems:'center'}}>
                    <FA name='smile-o' size={25}/>
                    <Image style={{width:20,height:20,marginLeft:10}} source={require('./../Resources/Icon/timeline_comment.png')}/>
                    <MCI style={{marginLeft:10}} name='share-variant' size={23}/>
                </View>
                <Text style={[{marginLeft:10},Theme.textSmall]}><FA name='globe'/> Yesterday at 2:30 PM</Text>
            </View>
        );
    }
}
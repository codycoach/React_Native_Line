import React, { Component } from 'react';
import {Image, ScrollView,TouchableOpacity,TouchableWithoutFeedback} from 'react-native';
import { Content,Container, Header, Tab, Tabs, TabHeading,Text, Icon, Left, Right, List, ListItem, Thumbnail, Body ,View} from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import FA from 'react-native-vector-icons/FontAwesome';
import Theme from './../../Styles/Theme';
export default class Friends extends Component {
  constructor(props){
    super(props)
    this.state={
      service:false,
      update:false,
      recommend:false,
      favorites:false,
      groups:false,
      friends:false
    }
  }

  _collapsibleService(){
    if(!this.state.service){
      this.setState({
        service:true
      })
    }else{
      this.setState({
        service:false
      })
    }
  }

  _collapsibleUpdate(){
    if(!this.state.update){
      this.setState({
        update:true
      })
    }else{
      this.setState({
        update:false
      })
    }
  }

  _collapsibleRecommend(){
    if(!this.state.recommend){
      this.setState({
        recommend:true
      })
    }else{
      this.setState({
        recommend:false
      })
    }
  }

  _collapsibleFavorites(){
    if(!this.state.favorites){
      this.setState({
        favorites:true
      })
    }else{
      this.setState({
        favorites:false
      })
    }
  }

  _collapsibleGroups(){
    if(!this.state.groups){
      this.setState({
        groups:true
      })
    }else{
      this.setState({
        groups:false
      })
    }
  }

  _collapsibleFriends(){
    if(!this.state.friends){
      this.setState({
        friends:true
      })
    }else{
      this.setState({
        friends:false
      })
    }
  }

  render() {
    return (
      <Container>
        <Content>
          {/* User */}
          <View style={{marginLeft:10,borderBottomWidth:1,borderColor:'gainsboro'}}>
            <View style={{flexDirection:'row',paddingBottom:10,paddingTop:10}}>
              <Thumbnail source={require('./../Resources/Image/user.jpg')} />
              <View style={{marginLeft:10}}>
                <Text style={{fontWeight:'700'}}>Coach</Text>
                <Text note>love</Text>
                <View style={{flexDirection:'row'}}>
                  <View style={{alignSelf:'center',borderWidth:1,borderColor:'silver',height:15,paddingLeft:3,paddingRight:3,paddingBottom:0,borderRadius:5,marginRight:5}}>
                    <Text note style={{fontSize:10}}>ID</Text>
                  </View>
                  <Text note>coach_etn</Text>
                </View>
              </View>
            </View>
          </View>
          {/* Service */}
          <View style={{borderBottomWidth:1,borderColor:'gainsboro',marginLeft:10}}>
            <TouchableOpacity activeOpacity={1} onPress={()=>this._collapsibleService()} style={{flexDirection:'row',padding:5}}>
              <View style={{flex:1,marginTop:5}}>
                <Text style={{color:'grey',fontSize:13,fontWeight:'bold'}}>LINE apps and services</Text>
              </View>
              <View style={{flex:1,alignItems:'flex-end'}}>
                <FA size={20} style={this.state.service?Theme.colorGrey:Theme.colorGreen} name={this.state.service?'angle-up':'angle-down'}/>
              </View>
            </TouchableOpacity>
            {this.state.service?<Service/>:null}
          </View>
          {/* Update */}
          <View style={{borderBottomWidth:1,borderColor:'gainsboro',marginLeft:10}}>
            <TouchableOpacity activeOpacity={1} onPress={()=>this._collapsibleUpdate()} style={{flexDirection:'row',padding:5}}>
              <View style={{flex:1,marginTop:5}}>
                <Text style={{color:'grey',fontSize:13,fontWeight:'bold'}}>Recently updated profiles 3</Text>
              </View>
              <View style={{flex:1,alignItems:'flex-end'}}>
                <FA size={20} style={this.state.update?Theme.colorGrey:Theme.colorGreen} name={this.state.update?'angle-up':'angle-down'}/>
              </View>
            </TouchableOpacity>
            {this.state.update?<Update/>:null}
          </View>
          {/* Reccommend */}
          <View style={{borderBottomWidth:1,borderColor:'gainsboro',marginLeft:10}}>
            <TouchableOpacity activeOpacity={1} onPress={()=>this._collapsibleRecommend()} style={{flexDirection:'row',padding:5}}>
              <View style={{flex:1,marginTop:5}}>
                <Text style={{color:'grey',fontSize:13,fontWeight:'bold'}}>Friend recommendations</Text>
              </View>
              <View style={{flex:1,alignItems:'flex-end'}}>
                <FA size={20} style={this.state.recommend?Theme.colorGrey:Theme.colorGreen} name={this.state.recommend?'angle-up':'angle-down'}/>
              </View>
            </TouchableOpacity>
            {this.state.recommend?<Recommend/>:null}
          </View>
          {/* Favorites */}
          <View style={{borderBottomWidth:1,borderColor:'gainsboro',marginLeft:10}}>
            <TouchableOpacity activeOpacity={1} onPress={()=>this._collapsibleFavorites()} style={{flexDirection:'row',padding:5}}>
              <View style={{flex:1,marginTop:5}}>
                <Text style={{color:'grey',fontSize:13,fontWeight:'bold'}}>Favorites 1</Text>
              </View>
              <View style={{flex:1,alignItems:'flex-end'}}>
                <FA size={20} style={this.state.favorites?Theme.colorGrey:Theme.colorGreen} name={this.state.favorites?'angle-up':'angle-down'}/>
              </View>
            </TouchableOpacity>
            {this.state.favorites?<Favorites/>:null}
          </View>
          {/* Groups */}
          <View style={{borderBottomWidth:1,borderColor:'gainsboro',marginLeft:10}}>
            <TouchableOpacity activeOpacity={1} onPress={()=>this._collapsibleGroups()} style={{flexDirection:'row',padding:5}}>
              <View style={{flex:1,marginTop:5}}>
                <Text style={{color:'grey',fontSize:13,fontWeight:'bold'}}>Groups 2</Text>
              </View>
              <View style={{flex:1,alignItems:'flex-end'}}>
                <FA size={20} style={this.state.groups?Theme.colorGrey:Theme.colorGreen} name={this.state.groups?'angle-up':'angle-down'}/>
              </View>
            </TouchableOpacity>
            {this.state.groups?<Groups/>:null}
          </View>
          {/* Friends */}
          <View style={{borderBottomWidth:1,borderColor:'gainsboro',marginLeft:10}}>
            <TouchableOpacity activeOpacity={1} onPress={()=>this._collapsibleFriends()} style={{flexDirection:'row',padding:5}}>
              <View style={{flex:1,marginTop:5}}>
                <Text style={{color:'grey',fontSize:13,fontWeight:'bold'}}>Friends 4</Text>
              </View>
              <View style={{flex:1,alignItems:'flex-end'}}>
                <FA size={20} style={this.state.friends?Theme.colorGrey:Theme.colorGreen} name={this.state.friends?'angle-up':'angle-down'}/>
              </View>
            </TouchableOpacity>
            {this.state.friends?<Friend/>:null}
          </View>
        </Content>
      </Container>
    );
  }
}

class Service extends Component {
  render() {
    return (
      <ScrollView horizontal style={{marginLeft:-10,marginBottom:10}}>
        <ServiceItem source = {require('./../Resources/Icon/frineds_sticker_shop.png')} text = {'Sticker\nShop'}/>
        <ServiceItem source = {require('./../Resources/Icon/friends_theme_shop.png')} text = {'Theme\nShop'}/>
        <ServiceItem source = {require('./../Resources/Icon/friends_gift_shop.png')} text = {'GIFT-\nSHOP'}/>
        <ServiceItem source = {require('./../Resources/Icon/friends_official_accounts.png')} text = {'Official\nAccounts'}/>
        <ServiceItem source = {require('./../Resources/Icon/friends_line_games.png')} text = {'LINE\ngames'}/>
        <ServiceItem source = {require('./../Resources/Icon/friends_line_rangers.png')} text = {'LINE\nRangers'}/>
        <ServiceItem source = {require('./../Resources/Icon/friends_line_bubble2.png')} text = {'LINE\nBUBBLE2'}/>
        <ServiceItem source = {require('./../Resources/Icon/friends_line_man.png')} text = {'LINE\nMAN'}/>
        <ServiceItem source = {require('./../Resources/Icon/friends_line_mobile.png')} text = {'LINE\nMOBILE'}/>
        <ServiceItem source = {require('./../Resources/Icon/friends_keep.png')} text = {'Keep'}/>
        <ServiceItem source = {require('./../Resources/Icon/friends_nearby.png')} text = {'Nearby'}/>
      </ScrollView>
    );
  }
}

class ServiceItem extends Component {
  render() {
    let {source,text} = this.props;
    return (
      <View style={{alignItems:'center',marginLeft:10}}>
        <Image style={Theme.friendService} source={source}/>
        <Text style={[Theme.textSmall,{textAlign:'center'}]}>{text}</Text>
      </View>
    );
  }
}

class Update extends Component {
  render() {
    return (
      <View>
      <FriendsItem source = {require('./../Resources/Image/fuse.jpg')} name = 'Fuse Rhazic' status = "หล่อเฟี้ยว เจียวไข่อร่อย"/>
        <FriendsItem source = {require('./../Resources/Image/tide.jpg')} name = 'Keatiyos Yothinraungrong' status = "Call me Lakelz"/>
        <FriendsItem source = {require('./../Resources/Image/tlek.jpg')} name = 'Tlek Metta' status = "จู่ๆ ก็วูปไป"/>
      </View>
    );
  }
}

class Recommend extends Component {
  render() {
    return (
      <View>
        <FriendsItem source = {require('./../Resources/Image/gordon.jpg')} name = 'Gordon Ramsay, Taylor Swift,...' right update/>
      </View>
    );
  }
}

class Favorites extends Component {
  render() {
    return (
      <View>
      <FriendsItem source = {require('./../Resources/Image/fuse.jpg')} name = 'Fuse Rhazic' status = "หล่อเฟี้ยว เจียวไข่อร่อย"/>
      </View>
    );
  }
}

class Groups extends Component {
  render() {
    return (
      <View>
        <FriendsItem source = {require('./../Resources/Image/dota.jpg')} name = 'Dota 2' update/>
        <FriendsItem source = {require('./../Resources/Image/sit.png')} name = 'SIT KMUTT' update/>
      </View>
    );
  }
}

class Friend extends Component {
  render() {
    return (
      <View>
        <FriendsItem source = {require('./../Resources/Icon/friends_official_accounts.png')} name = 'Official accounts' right update/>
        <FriendsItem source = {require('./../Resources/Image/fuse.jpg')} name = 'Fuse Rhazic' status = "หล่อเฟี้ยว เจียวไข่อร่อย"/>
        <FriendsItem source = {require('./../Resources/Image/samuch.jpg')} name = 'สะสะสะสาา มารถ' update/>
        <FriendsItem source = {require('./../Resources/Image/tide.jpg')} name = 'Keatiyos Yothinraungrong' status = "Call me Lakelz"/>
        <FriendsItem source = {require('./../Resources/Image/tlek.jpg')} name = 'Tlek Metta' status = "จู่ๆ ก็วูปไป"/>
      </View>
    );
  }
}

class FriendsItem extends Component {
  render() {
    let {source,name,status,right,update} = this.props
    return (
      <View style={{flexDirection:'row',padding:5}}>
        <Thumbnail source={source}/>
        {update?null:<Text style={{marginTop:-55,marginLeft:-5,color:'#53B535',fontSize:60,fontFamily:'Arial'}}>.</Text>}
        <View style={{marginLeft:10,justifyContent:'center'}}>
          <Text>{name}</Text>
          {status?<Text note>{status}</Text>:null}
        </View>
        {right?
          <View style={{justifyContent:'center',flex:1}}>
            <Text style={{textAlign:'right'}}>6 <FA  size={20} style={Theme.colorGrey} name="angle-right"/></Text>
          </View>
          :null
        }
      </View>
    );
  }
}